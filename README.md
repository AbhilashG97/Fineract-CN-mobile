### Status

[![Join the chat at https://gitter.im/therajanmaurya/android-client-2.0](https://badges.gitter.im/therajanmaurya/android-client-2.0)](https://gitter.im/therajanmaurya/android-client-2.0?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)
[![Build Status](https://travis-ci.org/therajanmaurya/android-client-2.0.svg?branch=development)](https://travis-ci.org/therajanmaurya/android-client-2.0)

# Android Client for Apache Fineract Version 2.0

This is an Android Application built on top of the Apache Fineract Version 2.0 platform. This is a native Android Application written purely in Java.

# Wiki

https://github.com/therajanmaurya/android-client-2.0/wiki

